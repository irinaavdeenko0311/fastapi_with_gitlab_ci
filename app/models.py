from sqlalchemy import Column, Integer, String, Text

from app.database import Base


class Recipe(Base):
    """Модель БД, описывающая сущность 'Рецепт'"""

    __tablename__ = "recipe"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    cooking_time = Column(Integer)
    ingredients = Column(Text)
    description = Column(Text)
    views = Column(Integer, default=0)
