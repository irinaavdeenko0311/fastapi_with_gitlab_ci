import asyncio
import os
from operator import itemgetter
from typing import AsyncGenerator

from fastapi.testclient import TestClient
from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine
from sqlalchemy_utils.functions import database_exists

from app.database import Base
from app.main import app, get_async_session
from app.models import Recipe

recipes_test_data = [
    Recipe(
        name="Тестовое1",
        cooking_time=180,
        ingredients="Мясо, картофель, лук, морковь, свекла, капуста",
        description="Сварить бульон. Лук с морковью обжарить. "
        "Свеклу обжарить. Добавить овощи в бульон.",
    ),
    Recipe(
        name="Тестовое2",
        cooking_time=100,
        ingredients="Мясо, мука, яйца, вода, соль",
        description="Из муки, яйца, соли и воды приготовить тесто. "
        "Из мяса сделать фарш. Тесто раскатать, сделать кружочки. "
        "В каждый кружочек положить фарш, свернуть. Варить 10 мин.",
    ),
    Recipe(
        name="Тестовое3",
        cooking_time=30,
        ingredients="Творог, яйцо, сахар, мука, соль, масло",
        description="Творог смешиваем с сахаром, солью, мукой яйцом. "
        "Из творога формируем кружочки. Жарим на среднем огне.",
    ),
]

CUR_DIR = os.path.dirname(__file__)
DATABASE_URL = f"sqlite+aiosqlite:///{CUR_DIR}/test_cookbook.db"

engine = create_async_engine(DATABASE_URL, echo=True)
TestingSession = async_sessionmaker(engine, expire_on_commit=False)
session = TestingSession()


async def override_get_async_session() -> AsyncGenerator:
    async with TestingSession.begin() as session:
        try:
            yield session
        except BaseException:
            await session.rollback()
            raise
        finally:
            await session.close()


async def fill_db() -> None:
    if not database_exists(DATABASE_URL):
        async with engine.begin() as conn:
            await conn.run_sync(Base.metadata.create_all)
            for recipe in recipes_test_data:
                session.add(recipe)
            await session.commit()


asyncio.run(fill_db())

app.dependency_overrides[get_async_session] = override_get_async_session

client = TestClient(app)


def test_recipes() -> None:
    """Тест для проверки endpointa '/recipes'."""

    response = client.get("/recipes")
    correct_response = sorted(
        [
            {
                "name": recipe_obj.name,
                "cooking_time": recipe_obj.cooking_time,
                "views": 0,
            }
            for recipe_obj in recipes_test_data
        ],
        key=itemgetter("views", "cooking_time"),
    )

    assert response.status_code == 200
    assert response.json() == correct_response


def test_recipe() -> None:
    """Тест для проверки endpointa '/recipe/{id}'."""

    recipe_id = 1
    recipe = recipes_test_data[0]
    correct_response = {
        "name": recipe.name,
        "cooking_time": recipe.cooking_time,
        "ingredients": recipe.ingredients,
        "description": recipe.description,
    }

    response = client.get(f"/recipes/{recipe_id}")

    assert response.status_code == 200
    assert response.json() == correct_response


def test_recipe_views() -> None:
    """Тест для проверки увеличения просмотров при открытии рецепта."""

    recipe_id = 2
    views_correct_after = 1

    client.get(f"/recipes/{recipe_id}")
    response = client.get("/recipes")

    views_after = response.json()[0].get("views")

    assert response.status_code == 200
    assert views_after == views_correct_after
