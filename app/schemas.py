from pydantic import BaseModel, Field


class BaseRecipe(BaseModel):
    """Базовая схема сериализации модели Recipe."""

    name: str = Field(
        ...,
        title="Full name of the recipe.",
        min_length=3,
        max_length=100,
        examples=["Блины", "Солянка"],
    )
    cooking_time: int = Field(..., title="Cooking time in minutes.")


class RecipeForList(BaseRecipe):
    """Схема сериализации модели Recipe для вывода в списке всех рецептов."""

    views: int


class RecipeDetail(BaseRecipe):
    """Схема сериализации модели Recipe для вывода детальной информации."""

    ingredients: str = Field(..., title="List of ingredients for cooking.")
    description: str = Field(..., title="Cooking steps.")
