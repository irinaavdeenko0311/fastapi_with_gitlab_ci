from typing import AsyncGenerator, List, Sequence

from fastapi import Depends, FastAPI, Path
from sqlalchemy import update
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
from sqlalchemy_utils.functions import database_exists

from app.database import DATABASE_URL, Base, async_session, engine
from app.models import Recipe
from app.schemas import RecipeDetail, RecipeForList

recipes_data = [
    Recipe(
        name="Борщ",
        cooking_time=180,
        ingredients="Мясо, картофель, лук, морковь, свекла, капуста",
        description="Сварить бульон. Лук с морковью обжарить. "
        "Свеклу обжарить. Добавить овощи в бульон.",
    ),
    Recipe(
        name="Пельмени",
        cooking_time=100,
        ingredients="Мясо, мука, яйца, вода, соль",
        description="Из муки, яйца, соли и воды приготовить тесто. "
        "Из мяса сделать фарш. Тесто раскатать, сделать кружочки. "
        "В каждый кружочек положить фарш, свернуть. Варить 10 мин.",
    ),
    Recipe(
        name="Сырники",
        cooking_time=30,
        ingredients="Творог, яйцо, сахар, мука, соль, масло",
        description="Творог смешиваем с сахаром, солью, мукой яйцом. "
        "Из творога формируем кружочки. Жарим на среднем огне.",
    ),
]

app = FastAPI()

session = async_session()


async def get_async_session() -> AsyncGenerator:
    """Функция для создания асинхронного генератора сессии."""

    async with async_session.begin() as session:
        try:
            yield session
        except BaseException:
            await session.rollback()
            raise
        finally:
            await session.close()


@app.on_event("startup")
async def startup() -> None:
    """Действия при запуске приложения."""

    if not database_exists(DATABASE_URL):
        async with engine.begin() as conn:
            await conn.run_sync(Base.metadata.create_all)

            for recipe in recipes_data:
                session.add(recipe)

            await session.commit()


@app.on_event("shutdown")
async def shutdown() -> None:
    """Действия при выключении приложения."""

    await session.close()


@app.get("/recipes", response_model=List[RecipeForList])
async def recipes(db: AsyncSession = Depends(get_async_session)) -> Sequence:
    """Endpoint для вывода всех рецептов, содержащихся в БД."""

    result = await db.execute(
        select(Recipe).order_by(Recipe.views.desc(), Recipe.cooking_time)
    )
    return result.scalars().all()


@app.get("/recipes/{id}", response_model=RecipeDetail)
async def recipe(
    id: int = Path(..., title="Recipe ID.", ge=1, le=3),
    db: AsyncSession = Depends(get_async_session),
) -> Recipe | None:
    """Endpoint для вывода детальной информации о рецепте."""

    result = await db.execute(select(Recipe).filter(Recipe.id == id))
    await db.execute(
        update(Recipe).where(Recipe.id == id).values(views=Recipe.views + 1)
    )
    await db.commit()

    return result.scalars().first()
